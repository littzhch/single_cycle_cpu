module control(
    input [4:0] opcode,
    input [2:0] funct3,
    input [6:0] funct7,

    output reg reg_enable,
    output reg mem_enable,
    output reg [2:0] imm_type,
    output reg alu_mode,
    output reg branch_enable,
    output reg jump,

    output reg pc_mode,
    output reg alu_src1_sel,
    output reg alu_src2_sel,
    output reg reg_data_sel,
    output reg alu_mem_sel
);
    always @ (*) begin
        reg_enable <= 0;
        mem_enable <= 0;
        imm_type <= 0;
        alu_mode <= 0;
        branch_enable <= 0;
        jump <= 0;
        pc_mode <= 0;
        alu_src1_sel <= 0;
        alu_src2_sel <= 0;
        reg_data_sel <= 0;
        alu_mem_sel <= 0;
        if (opcode == 5'b01100) begin  // R type
            reg_enable <= 1;
            mem_enable <= 0;
            imm_type <= 0;
            alu_mode <= funct7 != 0;
            branch_enable <= 0;
            jump <= 0;
            pc_mode <= 0;
            alu_src1_sel <= 0;
            alu_src2_sel <= 0;
            reg_data_sel <= 0;
            alu_mem_sel <= 0;
        end
        else if (opcode == 5'b11000) begin  // B type
            reg_enable <= 0;
            mem_enable <= 0;
            imm_type <= 3'b010;
            alu_mode <= funct3 != 0;
            branch_enable <= 1;
            jump <= 0;
            pc_mode <= 0;
            alu_src1_sel <= 0;
            alu_src2_sel <= 0;
            reg_data_sel <= 0;
            alu_mem_sel <= 0;
        end
        else if (opcode == 5'b00100) begin  // I type (addi)
            reg_enable <= 1;
            mem_enable <= 0;
            imm_type <= 3'b000;
            alu_mode <= 0;
            branch_enable <= 0;
            jump <= 0;
            pc_mode <= 0;
            alu_src1_sel <= 0;
            alu_src2_sel <= 1;
            reg_data_sel <= 0;
            alu_mem_sel <= 0;
        end
        else if (opcode == 5'b00000) begin  // load (lw)
            reg_enable <= 1;
            mem_enable <= 0;
            imm_type <= 3'b000;
            alu_mode <= 0;
            branch_enable <= 0;
            jump <= 0;
            pc_mode <= 0;
            alu_src1_sel <= 0;
            alu_src2_sel <= 1;
            reg_data_sel <= 0;
            alu_mem_sel <= 1;
        end
        else if (opcode == 5'b01000) begin  // save (sw)
            reg_enable <= 0;
            mem_enable <= 1;
            imm_type <= 3'b001;
            alu_mode <= 0;
            branch_enable <= 0;
            jump <= 0;
            pc_mode <= 0;
            alu_src1_sel <= 0;
            alu_src2_sel <= 1;
            reg_data_sel <= 0;
            alu_mem_sel <= 0;
        end
        else if (opcode == 5'b00101) begin  // (auipc)
            reg_enable <= 1;
            mem_enable <= 0;
            imm_type <= 3'b011;
            alu_mode <= 0;
            branch_enable <= 0;
            jump <= 0;
            pc_mode <= 0;
            alu_src1_sel <= 1;
            alu_src2_sel <= 1;
            reg_data_sel <= 0;
            alu_mem_sel <= 0;
        end
        else if (opcode == 5'b11011) begin  // (jal)
            reg_enable <= 1;
            mem_enable <= 0;
            imm_type <= 3'b100;
            alu_mode <= 0;
            branch_enable <= 0;
            jump <= 1;
            pc_mode <= 0;
            alu_src1_sel <= 0;
            alu_src2_sel <= 0;
            reg_data_sel <= 1;
            alu_mem_sel <= 0;
        end
        else if (opcode == 5'b11001) begin  // (jalr)
            reg_enable <= 1;
            mem_enable <= 0;
            imm_type <= 3'b000;
            alu_mode <= 0;
            branch_enable <= 0;
            jump <= 1;
            pc_mode <= 1;
            alu_src1_sel <= 0;
            alu_src2_sel <= 1;
            reg_data_sel <= 1;
            alu_mem_sel <= 0;
        end
    end
endmodule