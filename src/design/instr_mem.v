module instr_mem(
    input [31:0] addr,
    output [31:0] data
);
    wire [7:0] raw_addr;
    assign raw_addr = (addr - 32'h0000_3000) >> 2;
    dist_mem_gen_instr dist_mem_instr(.a(raw_addr), .spo(data));
endmodule