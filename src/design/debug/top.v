module top(
    input clk,
    input sw7,
    input sw6,
    input sw5,
    input [4:0] sw4_0,
    input button,

    output led7,
    output [1:0] led6_5,
    output [4:0] led4_0,
    output [2:0] an,
    output [3:0] seg
);

    wire [7:0] io_addr;
    wire [31:0] io_dout;
    wire  io_we;
    wire [31:0] io_din;
    wire [7:0] m_rf_addr;
    wire [31:0] rf_data;
    wire [31:0] m_data;
    wire [31:0] pc;
    wire clk_cpu;
    wire rst_cpu;


    pdu_1cycle pdu_inst(
        .clk(clk),
        .rst(sw7),
        .run(sw6),
        .step(button),
        .clk_cpu(clk_cpu),
        .rst_cpu(rst_cpu),
        .valid(sw5),
        .in(sw4_0),
        .check(led6_5),
        .out0(led4_0),
        .an(an),
        .seg(seg),
        .ready(led7),
        .io_addr(io_addr),
        .io_dout(io_dout),
        .io_we(io_we),
        .io_din(io_din),
        .m_rf_addr(m_rf_addr),
        .rf_data(rf_data),
        .m_data(m_data),
        .pc(pc)
    );
    cpu cpu_inst(
        .clk(clk_cpu),
        .rst(rst_cpu),
        .io_addr(io_addr),
        .io_dout(io_dout),
        .io_we(io_we),
        .io_din(io_din),
        .m_rf_addr(m_rf_addr),
        .rf_data(rf_data),
        .m_data(m_data),
        .pc(pc)
    );
endmodule
