module program_counter(
    input clk,
    input rst,

    input branch,
    input pc_mode, // 0: pc_inc; 1: pc_new
    input [31:0] pc_inc,
    input [31:0] pc_new,
    
    output reg [31:0] pc,
    output wire [31:0] pc_add_4
);
    assign pc_add_4 = pc + 4;
    always @ (posedge clk, posedge rst) begin
        if (rst)
            pc <= 32'h0000_3000;
        else if (branch)
            if (pc_mode == 0)
                pc <= pc + pc_inc;
            else
                pc <= pc_new;
        else
            pc <= pc_add_4;
    end 
endmodule