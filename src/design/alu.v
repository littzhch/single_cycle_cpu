module alu (
    input [31:0] number1,
    input [31:0] number2,
    input mode, // 0: result = number1 + number2; branch_result = beq
                // 1: result = number1 - number2; branch_result = blt

    output [31:0] result,
    output branch_result
);
    assign result = mode ? number1 - number2 : number1 + number2;
    assign branch_result = 
        mode ? (
            number1[31] > number2[31]
            || number1[30:0] < number2[30:0]
        )
        : (
            number1 == number2
        );
endmodule