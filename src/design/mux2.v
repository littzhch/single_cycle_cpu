module mux2 #(parameter WIDTH = 32) (
    input [WIDTH - 1:0] data0,
    input [WIDTH - 1:0] data1,
    input select,
    output [WIDTH - 1:0] data_out
);
    assign data_out = select ? data1 : data0;
endmodule