module cpu(
     input clk,
     input rst,

    // IO bus
    output [7:0] io_addr,
    output [31:0] io_dout,
    output io_we,
    input [31:0] io_din,

    // debug bus
    input [7:0] m_rf_addr,
    output [31:0] rf_data,
    output [31:0] m_data,
    output [31:0] pc
);

    wire [31:0] pc_pc;
    wire [31:0] pc_pc_add_4;

    wire [31:0] instr_mem_data;

    wire [4:0] instr_decoder_opcode;
    wire [4:0] instr_decoder_rd;
    wire [4:0] instr_decoder_rs1;
    wire [4:0] instr_decoder_rs2;
    wire [2:0] instr_decoder_funct3;
    wire [6:0] instr_decoder_funct7;
    wire [24:0] instr_decoder_immsrc;

    wire [31:0] imm_gen_imm;

    wire [31:0] reg_file_out_data1;
    wire [31:0] reg_file_out_data2;

    wire [31:0] alu_result;
    wire alu_branch_result;

    wire [31:0] data_mem_data_out;

    wire [31:0] zero_lsb_data_out;

    wire [31:0] alu_src1_mux_out;
    wire [31:0] alu_src2_mux_out;
    wire [31:0] reg_data_mux_out;
    wire [31:0] alu_mem_mux_out;

    wire and_out;
    wire or_out;

    wire control_reg_enable;
    wire control_mem_enable;
    wire [2:0] control_imm_type;
    wire control_alu_mode;
    wire control_branch_enable;
    wire control_jump;
    wire control_pc_mode;
    wire control_alu_src1_sel;
    wire control_alu_src2_sel;
    wire control_reg_data_sel;
    wire control_alu_mem_sel;

    control control_inst(
        .opcode(instr_decoder_opcode),
        .funct3(instr_decoder_funct3),
        .funct7(instr_decoder_funct7),
        .reg_enable(control_reg_enable),
        .mem_enable(control_mem_enable),
        .imm_type(control_imm_type),
        .alu_mode(control_alu_mode),
        .branch_enable(control_branch_enable),
        .jump(control_jump),
        .pc_mode(control_pc_mode),
        .alu_src1_sel(control_alu_src1_sel),
        .alu_src2_sel(control_alu_src2_sel),
        .reg_data_sel(control_reg_data_sel),
        .alu_mem_sel(control_alu_mem_sel)
    );

    program_counter pc_inst(
        .clk(clk),
        .rst(rst),
        .branch(or_out),
        .pc_mode(control_pc_mode),
        .pc_inc(imm_gen_imm),
        .pc_new(zero_lsb_data_out),
        .pc(pc_pc),
        .pc_add_4(pc_pc_add_4)
    );

    instr_mem instr_mem_inst(
        .addr(pc_pc),
        .data(instr_mem_data)
    );

    instr_decoder instr_decoder_inst(
        .instr(instr_mem_data),
        .opcode(instr_decoder_opcode),
        .rd(instr_decoder_rd),
        .rs1(instr_decoder_rs1),
        .rs2(instr_decoder_rs2),
        .funct3(instr_decoder_funct3),
        .funct7(instr_decoder_funct7),
        .immsrc(instr_decoder_immsrc)
    );

    imm_gen imm_gen_inst(
        .instr(instr_decoder_immsrc),
        .imm_type(control_imm_type),
        .imm(imm_gen_imm)
    );

    register_file register_file_inst(
        .clk(clk),
        .read_reg1(instr_decoder_rs1),
        .out_data1(reg_file_out_data1),
        .read_reg2(instr_decoder_rs2),
        .out_data2(reg_file_out_data2),
        .write_reg(instr_decoder_rd),
        .write_data(reg_data_mux_out),
        .write_enable(control_reg_enable),
        .dbg_reg(m_rf_addr[4:0]),
        .dbg_data(rf_data)
    );

    alu alu_inst(
        .number1(alu_src1_mux_out),
        .number2(alu_src2_mux_out),
        .mode(control_alu_mode),
        .result(alu_result),
        .branch_result(alu_branch_result)
    );

    data_mem data_mem_inst(
        .clk(clk),
        .addr(alu_result),
        .data_in(reg_file_out_data2),
        .write_enable(control_mem_enable),
        .data_out(data_mem_data_out),
        .io_addr(io_addr),
        .io_dout(io_dout),
        .io_we(io_we),
        .io_din(io_din),
        .dbg_addr(m_rf_addr),
        .dbg_data_out(m_data)
    );

    zero_lsb zero_lsb_inst(
        .data_in(alu_result),
        .data_out(zero_lsb_data_out)
    );

    mux2 alu_src1_mux(
        .data0(reg_file_out_data1),
        .data1(pc_pc),
        .select(control_alu_src1_sel),
        .data_out(alu_src1_mux_out)
    );

    mux2 alu_src2_mux(
        .data0(reg_file_out_data2),
        .data1(imm_gen_imm),
        .select(control_alu_src2_sel),
        .data_out(alu_src2_mux_out)
    );

    mux2 reg_data_mux(
        .data0(alu_mem_mux_out),
        .data1(pc_pc_add_4),
        .select(control_reg_data_sel),
        .data_out(reg_data_mux_out)
    );

    mux2 alu_mem_mux(
        .data0(alu_result),
        .data1(data_mem_data_out),
        .select(control_alu_mem_sel),
        .data_out(alu_mem_mux_out)
    );

    and and_inst(and_out, alu_branch_result, control_branch_enable);
    or or_inst(or_out, control_jump, and_out);

    assign pc = pc_pc;
endmodule