module register_file (
    input clk,

    input [4:0] read_reg1,
    output [31:0] out_data1,

    input [4:0] read_reg2,
    output [31:0] out_data2,

    input [4:0] write_reg,
    input [31:0] write_data,
    input write_enable,

    // useless debug ports
    input [4:0] dbg_reg,
    output [31:0] dbg_data
);
    reg [31:0] registers[0:31];

    always @ (posedge clk) begin
        if (write_enable) begin
            registers[write_reg] <= write_data;
        end
    end
    assign out_data1 = read_reg1 == 0 ? 0 : registers[read_reg1];
    assign out_data2 = read_reg2 == 0 ? 0 : registers[read_reg2];
    assign dbg_data = dbg_reg == 0 ? 0 : registers[dbg_reg];
endmodule