module data_mem(
    input clk,

    // memory interface
    input [31:0] addr,
    input [31:0] data_in,
    input write_enable,
    output [31:0] data_out,

    // IO BUS
    output [7:0] io_addr,
    output [31:0] io_dout,
    output io_we,
    input [31:0] io_din,

    // debug interface
    input [7:0] dbg_addr,
    output [31:0] dbg_data_out
);

    wire [31:0] selector_mem_addr;
    wire [31:0] selector_mem_data_write;
    wire selector_mem_we;
    wire [31:0] data_store_data_out0;
    wire [2:0] selector_reg_id;
    wire [31:0] selector_reg_data_write;
    wire selector_reg_we;
    wire [31:0] io_registers_reg_dout;

    mem_io_selector mem_io_selector_inst(
        .addr(addr),
        .data_write(data_in),
        .we(write_enable),
        .data_read(data_out),
        .mem_addr(selector_mem_addr),
        .mem_data_write(selector_mem_data_write),
        .mem_we(selector_mem_we),
        .mem_data_read(data_store_data_out0),
        .reg_id(selector_reg_id),
        .reg_data_write(selector_reg_data_write),
        .reg_we(selector_reg_we),
        .reg_data_read(io_registers_reg_dout)
    );

    data_store data_store_inst(
        .clk(clk),
        .addr0(selector_mem_addr),
        .n_bytes(3),
        .ext_mode(1),
        .data_in(selector_mem_data_write),
        .data_out0(data_store_data_out0),
        .we(selector_mem_we),
        .addr1(dbg_addr),
        .data_out1(dbg_data_out)
    );

    io_registers io_registers_inst(
        .clk(clk),
        .reg_id(selector_reg_id),
        .reg_din(selector_reg_data_write),
        .reg_dout(io_registers_reg_dout),
        .reg_we(selector_reg_we),
        .io_addr(io_addr),
        .io_dout(io_dout),
        .io_we(io_we),
        .io_din(io_din)
    );
endmodule


module data_store(
    input clk,
    input [31:0] addr0,
    input [1:0] n_bytes,
    input ext_mode,  // 1: 符号扩展  0: 零扩展
    input [31:0] data_in,
    output [31:0] data_out0,
    input we,
    input [31:0] addr1,
    output [31:0] data_out1

    // n_bytes: 读取或写入的数据字节数 - 1，最高为3，即读写4个字节
    // 低字节和低地址对应，高字节和高地址对应
    // 不需要数据对齐
);
    wire w_we0, w_we1, w_we2, w_we3;
    wire [7:0] addr_mem_0;
    wire [7:0] addr_mem_1;
    wire [7:0] addr_mem_2;
    wire [7:0] addr_mem_3;
    wire [7:0] addr_dbg_0;
    wire [7:0] addr_dbg_1;
    wire [7:0] addr_dbg_2;
    wire [7:0] addr_dbg_3;
    wire [7:0] w_wdata_0;
    wire [7:0] w_wdata_1;
    wire [7:0] w_wdata_2;
    wire [7:0] w_wdata_3;
    wire [7:0] rdata_mem_0;
    wire [7:0] rdata_mem_1;
    wire [7:0] rdata_mem_2;
    wire [7:0] rdata_mem_3;
    wire [7:0] rdata_dbg_0;
    wire [7:0] rdata_dbg_1;
    wire [7:0] rdata_dbg_2;
    wire [7:0] rdata_dbg_3;


    addr_data_dist addr_data_dist_mem(
        .raw_addr(addr0),
        .wdata(data_in),
        .rdata(data_out0),
        .n_bytes(n_bytes),
        .ext_mode(ext_mode),
        .we(we),
        .addr0(addr_mem_0),
        .addr1(addr_mem_1),
        .addr2(addr_mem_2),
        .addr3(addr_mem_3),
        .wdata0(w_wdata_0),
        .wdata1(w_wdata_1),
        .wdata2(w_wdata_2),
        .wdata3(w_wdata_3),
        .rdata0(rdata_mem_0),
        .rdata1(rdata_mem_1),
        .rdata2(rdata_mem_2),
        .rdata3(rdata_mem_3),
        .we0(w_we0),
        .we1(w_we1),
        .we2(w_we2),
        .we3(w_we3)
    );

    addr_data_dist addr_data_dist_dbg(
        .raw_addr(addr1),
        .wdata(0),
        .rdata(data_out1),
        .n_bytes(3),
        .ext_mode(0),
        .we(0),
        .addr0(addr_dbg_0),
        .addr1(addr_dbg_1),
        .addr2(addr_dbg_2),
        .addr3(addr_dbg_3),
        .wdata0(),
        .wdata1(),
        .wdata2(),
        .wdata3(),
        .rdata0(rdata_dbg_0),
        .rdata1(rdata_dbg_1),
        .rdata2(rdata_dbg_2),
        .rdata3(rdata_dbg_3),
        .we0(),
        .we1(),
        .we2(),
        .we3()
    );

    dist_mem_gen_data0 mem0(
        .clk(clk),
        .a(addr_mem_0),
        .d(w_wdata_0),
        .spo(rdata_mem_0),
        .we(w_we0),
        .dpra(addr_dbg_0),
        .dpo(rdata_dbg_0)
    );

    dist_mem_gen_data1 mem1(
        .clk(clk),
        .a(addr_mem_1),
        .d(w_wdata_1),
        .spo(rdata_mem_1),
        .we(w_we1),
        .dpra(addr_dbg_1),
        .dpo(rdata_dbg_1)
    );

    dist_mem_gen_data2 mem2(
        .clk(clk),
        .a(addr_mem_2),
        .d(w_wdata_2),
        .spo(rdata_mem_2),
        .we(w_we2),
        .dpra(addr_dbg_2),
        .dpo(rdata_dbg_2)
    );

    dist_mem_gen_data3 mem3(
        .clk(clk),
        .a(addr_mem_3),
        .d(w_wdata_3),
        .spo(rdata_mem_3),
        .we(w_we3),
        .dpra(addr_dbg_3),
        .dpo(rdata_dbg_3)
    );
endmodule

module addr_data_dist(
    input [31:0] raw_addr,
    input [31:0] wdata,
    output [31:0] rdata,
    input [1:0] n_bytes,
    input ext_mode, // 1: 符号扩展; 0: 零扩展
    input we,

    output [7:0] addr0,
    output [7:0] addr1,
    output [7:0] addr2,
    output [7:0] addr3,

    output [7:0] wdata0,
    output [7:0] wdata1,
    output [7:0] wdata2,
    output [7:0] wdata3,

    input [7:0] rdata0,
    input [7:0] rdata1,
    input [7:0] rdata2,
    input [7:0] rdata3,

    output we0,
    output we1,
    output we2,
    output we3
);

wire selected[0:3];       // selected[n]: 编号为n的存储器是否需要读或写
wire [1:0] mem_no[0:3];   // mem_pos[n]: 总数据中的第n个字节对应的存储器编号
wire [1:0] byte_pos[0:3]; // byte_pos[n]: 第n个存储器对应的总数据中的字节序号
wire rdata_sign;          // 读取数据的符号
wire [7:0] wbytes [3:0];
wire [7:0] rbytes [3:0];
wire [7:0] mem_rbytes [0:3];

assign rdata_sign = mem_rbytes[raw_addr[1:0] + n_bytes][7:7];

assign selected[0] = 2'b00 - raw_addr[1:0] <= n_bytes;
assign selected[1] = 2'b01 - raw_addr[1:0] <= n_bytes;
assign selected[2] = 2'b10 - raw_addr[1:0] <= n_bytes;
assign selected[3] = 2'b11 - raw_addr[1:0] <= n_bytes;

assign mem_no[0] = raw_addr[1:0];
assign mem_no[1] = raw_addr[1:0] + 1;
assign mem_no[2] = raw_addr[1:0] + 2;
assign mem_no[3] = raw_addr[1:0] + 3;

assign byte_pos[0] = 2'b00 - raw_addr[1:0];
assign byte_pos[1] = 2'b01 - raw_addr[1:0];
assign byte_pos[2] = 2'b10 - raw_addr[1:0];
assign byte_pos[3] = 2'b11 - raw_addr[1:0];

assign wbytes[0] = wdata[7:0];
assign wbytes[1] = wdata[15:8];
assign wbytes[2] = wdata[23:16];
assign wbytes[3] = wdata[31:24];
assign rdata = {rbytes[3], rbytes[2], rbytes[1], rbytes[0]};
assign mem_rbytes[0] = rdata0;
assign mem_rbytes[1] = rdata1;
assign mem_rbytes[2] = rdata2;
assign mem_rbytes[3] = rdata3;


assign addr0 = (raw_addr + {30'd0, (2'b00 - raw_addr[1:0])}) >> 2;
assign addr1 = (raw_addr + {30'd0, (2'b01 - raw_addr[1:0])}) >> 2;
assign addr2 = (raw_addr + {30'd0, (2'b10 - raw_addr[1:0])}) >> 2;
assign addr3 = (raw_addr + {30'd0, (2'b11 - raw_addr[1:0])}) >> 2;

assign wdata0 = wbytes[byte_pos[0]];
assign wdata1 = wbytes[byte_pos[1]];
assign wdata2 = wbytes[byte_pos[2]];
assign wdata3 = wbytes[byte_pos[3]];

assign rbytes[0] = mem_rbytes[mem_no[0]];
assign rbytes[1] = n_bytes >= 1 ? mem_rbytes[mem_no[1]] : {8{ext_mode & rdata_sign}};
assign rbytes[2] = n_bytes >= 2 ? mem_rbytes[mem_no[2]] : {8{ext_mode & rdata_sign}};
assign rbytes[3] = n_bytes >= 3 ? mem_rbytes[mem_no[3]] : {8{ext_mode & rdata_sign}};

assign we0 = selected[0] & we;
assign we1 = selected[1] & we;
assign we2 = selected[2] & we;
assign we3 = selected[3] & we;
endmodule


module io_registers(
    input clk,

    // memory interface
    input [2:0] reg_id,
    input [31:0] reg_din,
    output [31:0] reg_dout,
    input reg_we,

    // IO interface
    output [7:0] io_addr,
    output [31:0] io_dout,
    output io_we,
    input [31:0] io_din
);
    reg [31:0] output_regs[0:2];
    always @ (posedge clk) begin
        if (reg_we && reg_id <= 2) begin
            output_regs[reg_id] <= reg_din;
        end
    end

    assign io_addr = {5'b0, reg_id} * 4;
    assign io_we = reg_we;
    assign io_dout = reg_din;
    assign reg_dout = reg_id <= 2 ? output_regs[reg_id] : io_din;
endmodule


module mem_io_selector(
    // 统一接口
    input [31:0] addr,
    input [31:0] data_write,
    input we,
    output [31:0] data_read,

    // data_store 接口
    output [31:0] mem_addr,
    output [31:0] mem_data_write,
    output mem_we,
    input [31:0] mem_data_read,

    // io_registers 接口
    output [2:0] reg_id,
    output [31:0] reg_data_write,
    output reg_we,
    input [31:0] reg_data_read
);
    assign mem_we = addr < 32'h0000_0400 ? we : 0;
    assign reg_we = addr >= 32'h0000_0400 ? we : 0;
    assign data_read = addr < 32'h0000_0400 ? mem_data_read : reg_data_read;

    assign mem_data_write = data_write;
    assign reg_data_write = data_write;

    assign mem_addr = addr;
    assign reg_id = (addr - 32'h0000_0400) >> 2;
endmodule