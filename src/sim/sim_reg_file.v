// sim result:
// http://home.ustc.edu.cn/~chi_zhang/picgo/2022/04/54a45c2abd1e1124/20220410121501.png

module tb_register_file;
    reg clk;

    reg [4:0] read_reg1;
    wire [31:0] out_data1;

    reg [4:0] read_reg2;
    wire [31:0] out_data2;

    reg [4:0] write_reg;
    reg [31:0] write_data;
    reg write_enable;

    // useless debug ports
    reg [4:0] dbg_reg;
    wire [31:0] dbg_data;

    register_file register_file_test(
        clk,
        read_reg1, out_data1,
        read_reg2, out_data2,
        write_reg, write_data, write_enable,
        dbg_reg, dbg_data
    );

    initial begin
        clk = 0;
        read_reg1 = 0;
        read_reg2 = 0;
        write_reg = 0;
        write_data = 0;
        write_enable = 0;
        dbg_reg = 0;
    end

    initial begin
        forever #20 clk = ~clk;
    end

    initial begin : _
        integer i;
        #40;
        for (i = 0; i < 32; i = i + 1) begin
            write_data = i + 1;
            write_reg = i;
            write_enable = 1;
            #40;
            write_data = 0;
            write_reg = 0;
            write_enable = 0;
            #40;
        end

        #37;
        for (i = 0; i < 32; i = i + 1) begin
            read_reg1 = i;
            #13 read_reg2 = i;
            #7 dbg_reg = i;
            #37;
        end

        $finish;
    end
endmodule