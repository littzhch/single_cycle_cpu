module tb_instr_mem;
    reg [31:0] addr;
    wire [31:0] data;

    instr_mem instr_mem_test(addr, data);

    initial begin : _
        integer i;
        addr = 32'h0000_3000;
        for (i = 0; i < 256; i = i + 1) begin
            #50;
            addr = addr + 4;
        end
        #50 $finish;
    end
endmodule