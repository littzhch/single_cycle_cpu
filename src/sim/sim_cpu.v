module tb_cpu;
    reg clk, rst;

    cpu cpu_test(clk, rst);
    
    initial begin
        clk = 0;
        forever #20 clk = ~clk;
    end
    initial begin
        rst = 1;
        #50;
        rst = 0;
    end
endmodule