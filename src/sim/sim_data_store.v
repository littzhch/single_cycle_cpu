module tb_data_store;
    reg clk;
    reg [31:0] addr0;
    reg [1:0] n_bytes;
    reg ext_mode;  // 1: 符号扩展  0: 零扩展
    reg [31:0] data_in;
    wire [31:0] data_out0;
    reg we;
    reg [31:0] addr1;
    wire [31:0] data_out1;

    data_store data_store_test(
        clk, addr0, n_bytes, ext_mode, data_in, data_out0,
        we, addr1, data_out1
    );

    initial begin
        clk = 0;
        addr0 = 1;
        n_bytes = 0;
        ext_mode = 1;
        data_in = 0;
        we = 0;
        addr1 = 3;
        forever #20 clk = ~clk;
    end

    initial begin
        data_in = 32'haabbccff;
        n_bytes = 3;
        we = 1;
        #100;
        n_bytes = 0;
        we = 0;
        #50 $finish;
    end
endmodule