// sim result:
// http://home.ustc.edu.cn/~chi_zhang/picgo/2022/04/f9544d05edd443a8/20220410113638.png

module tb_alu;
    reg [31:0] number1;
    reg [31:0] number2;
    reg mode;   // 0: result = number1 + number2; branch_result = beq
                // 1: result = number1 - number2; branch_result = blt

    wire [31:0] result;
    wire branch_result;

    alu alu_test(number1, number2, mode, result, branch_result);

    initial begin
        number1 = 0;
        number2 = 0;
        mode = 0;
    end

    initial begin
        #20 number1 = 3;
        number2 = 68;

        #20 mode = 1;
        #20 mode = 0;

        #20 number1 = -1;
        number2 = 22;

        #20 mode = 1;
        #20 mode = 0;

        #20 number1 = -4;
        number2 = -8;
        
        #20 mode = 1;
        #20 mode = 0;

        #20 number1 = -1332;
        number2 = -1332;
        
        #20 mode = 1;
        #20 mode = 0;

        #20 number1 = -1;
        number2 = 0;
        
        #20 mode = 1;
        #20 mode = 0;

        #20 $finish;
    end
endmodule