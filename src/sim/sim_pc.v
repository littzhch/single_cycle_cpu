// sim result:
// http://home.ustc.edu.cn/~chi_zhang/picgo/2022/04/ede6b93c2bf56d6e/20220410164948.png

module tb_program_counter;
    reg clk;
    reg rst;

    reg branch;
    reg [31:0] pc_inc;
    
    wire [31:0] pc;
    wire [31:0] pc_add_4;

    program_counter program_counter_test(clk, rst, branch, pc_inc, pc, pc_add_4);

    initial begin
        clk = 0;
        forever #20 clk = ~clk;
    end

    initial begin
        rst = 1;
        branch = 0;
        pc_inc = 0;
        #80;
        rst = 0;
    end

    initial begin
        #240;
        pc_inc = 16;
        branch = 1;
        #160;
        branch = 1;
        pc_inc = -8;
        #160 $finish;
    end
endmodule