module tb_top;
    reg clk;
    reg sw7;
    reg sw6;
    reg sw5;
    reg [4:0] sw4_0;
    reg button;

    wire led7;
    wire [1:0] led6_5;
    wire [4:0] led4_0;
    wire [2:0] an;
    wire [3:0] seg;

    top top_test(
        clk,
        sw7,
        sw6,
        sw5,
        sw4_0,
        button,
        led7,
        led6_5,
        led4_0,
        an,
        seg
    );

    initial begin
        clk = 0;
        sw7 = 0;
        sw6 = 0;
        sw5 = 0;
        sw4_0 = 0;
        button = 0;
    end

    initial begin
        forever #10 clk = ~clk;
    end

    initial begin
        sw7 = 1;
        #80 sw7 = 0;
        #20 sw6 = 1;
        #100 sw5 = 1;
        #150 sw5 = 0;
    end
endmodule