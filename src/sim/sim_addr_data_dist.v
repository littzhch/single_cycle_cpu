module tb_addr_data_dist;
    reg [31:0] raw_addr;
    reg [31:0] wdata;
    wire [31:0] rdata;
    reg [1:0] n_bytes;
    reg ext_mode; // 1: 符号扩展; 0: 零扩展
    reg we;

    wire [7:0] addr0;
    wire [7:0] addr1;
    wire [7:0] addr2;
    wire [7:0] addr3;

    wire [7:0] wdata0;
    wire [7:0] wdata1;
    wire [7:0] wdata2;
    wire [7:0] wdata3;

    reg [7:0] rdata0;
    reg [7:0] rdata1;
    reg [7:0] rdata2;
    reg [7:0] rdata3;

    wire we0;
    wire we1;
    wire we2;
    wire we3;

    addr_data_dist addr_data_dist_test(
        raw_addr,
        wdata,
        rdata,
        n_bytes,
        ext_mode,
        we,
        addr0,
        addr1,
        addr2,
        addr3,
        wdata0,
        wdata1,
        wdata2,
        wdata3,
        rdata0,
        rdata1,
        rdata2,
        rdata3,
        we0,
        we1,
        we2,
        we3
    );

    initial begin
        raw_addr = 0;
        wdata = 32'hddccbbaa;
        n_bytes = 0;
        ext_mode = 1;
        we = 0;

        rdata0 = 8'hff;
        rdata1 = 1;
        rdata2 = 2;
        rdata3 = 3;
    end

    initial begin
        we = 1;
        #20 n_bytes = 1;
        #20 n_bytes = 2;
        #20 raw_addr = 1;
        #20 raw_addr = 2;
        #20 raw_addr = 3;
        #20 raw_addr = 4;
        #20 raw_addr = 5;
        #20 raw_addr = 6;
        #20 n_bytes = 3;
        #20 raw_addr = 7;
        #20 raw_addr = 8;
    end
endmodule